---
layout: markdown_page
title: "Category Direction - Kubernetes Management"
description: "GitLab aim to provide a simple way for users to configure their clusters and manage Kubernetes. Learn more here!"
canonical_path: "/direction/configure/kubernetes_management/"
---

- TOC
{:toc}

## Overview

The main motivating factor behind [using a container orchestration platform is cost reduction](https://medium.com/kubernetes-tutorials/how-can-containers-and-kubernetes-save-you-money-fc66b0c94022). Kubernetes has become the most widely adopted container orchestration platform to run, deploy, and manage applications. Yet, to seamlessly connect application code with the clusters in various environments, has remained a complex task that needs various approaches of CI/CD and Infrastructure as Code. 

Our mission is to support the Platform Engineers in enabling developer workflows, to make deployments to every environment frictionless for the developer and reliable for the operator, no matter the experience level. We do this by supporting an integrated experience within GitLab and leverage the existing tools and approaches in the Kubernetes community.

### Market overview

Kubernetes is emerging as the _de facto_ standard for container orchestration. Today, Kubernetes runs in half of container environments, and around 90% of containers are orchestrated. ([Source](https://www.datadoghq.com/container-report/)).

### How can you contribute

Interested in joining the conversation for this category? Please have a look at our [global epic](https://gitlab.com/groups/gitlab-org/-/epics/115) where
we discuss this topic and can answer any questions you may have or direct your attention to one of the more targeted epics presented below where our focus is today.
Your contributions are more than welcome.

## Vision

By 2022, GitLab is an indispensable addition in the workflow of Kubernetes users by making deployments reliable, frictionless and conventional, and by surfacing in-cluster insights in the form of alerts, metrics and logs in GitLab. As we are primarily focusing on enterprise customers, we want to support highly regulated, air-gapped use cases along with generic, public-cloud based environments.

## Target User

We are building GitLab's Kubernetes Management category for the enterprise operators, who are already operating production grade clusters and work hard to remove all the roadblocks that could slow down developers without putting operations at risk. Our secondary persona is the Application Operator, who is in charge of 2nd day operations of deployed applictions in production environments.

- [Priyanka, the platform engineer](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#priyanka-platform-engineer)
- [Allison, the application operator](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#allison-application-ops)

## Current status

We support 2 approaches to connect a cluster with GitLab:

- attaching a cluster to GitLab [using a cluster certificate](https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html)
- using the [GitLab Kubernetes Agent](https://docs.gitlab.com/ee/user/clusters/agent/)

Beside these, one can manually configur a `$KUBECONFIG` or use 3rd party tooling around GitLab CI or webhooks.

The recommended approach to integrate a Kubernetes cluster with GitLab is using the [GitLab Kubernetes Agent](https://docs.gitlab.com/ee/user/clusters/agent/).

In the case of the certificate based integration, GitLab should be able to access the kubernetes cluster APIs. This requirement is not acceptable in many use cases, especially on gitlab.com SaaS as it's considered a big security risk to open up the Kube API towards the Internet. For this reason, we are focusing on building out various integrations using the GitLab Kubernetes Agent that is secure by design.

Using the [GitLab Kubernetes Agent](https://docs.gitlab.com/ee/user/clusters/agent/), we want to offer a security oriented, Kubernetes-friendly integration option to Platform Engineers. Beside providing more control for the cluster operator, the agent opens up many new possibilities to make GitLab - cluster integrations easier and deeper. We expect the agent to support various GitLab features with automatic setups and integrations.

Today, the Agent supports the following workflows:

- pull based deployments built on top of the `cli-utils` from Google
- push based deployments using the GitLab Runners, this provides support for many other features, like Auto Deploy
- network security alert integrations with Cillium 

## What's next & why

We are working towards supporting the most important and valuable features of the certificate based Kubernetes integration with the GitLab Kubernetes Agent. At the same time, we do not aim for feature parity, as we believe that some features did not serve our users well. Morevoer, we don't plan the deprecatiion of the certificate based integration yet.

We invite you to [share your feedback and join the discussion](https://gitlab.com/gitlab-org/gitlab/-/issues/216569) on the future of our Kubernetes integrations.

#### Simplifying getting started with the Agent

The GitLab Kubernetes Agent is the foundational building block of our future Kubernetes integrations, but in its own it comes without features and without user interfaces. This is clearly not what we want, so we are focusing on adding user interfaces to [provide administrative, connection-related insights between GitLab and the cluster](https://gitlab.com/groups/gitlab-org/-/epics/4739), and more importantly to [provide insights about ongoing deployments and the resources managed by an Agent](https://gitlab.com/gitlab-org/gitlab/-/issues/258603).

### GitLab Integrated Applications

Many GitLab features rely on 3rd party applications being available in the user's clusters. We are changing our approach to GitLab Managed Apps to GitLab Integrated Apps where platform operators can bring their own applications and processes, and GitLab can integrate with them to provide its features. As a result, we are moving away from the one click, UI based installation, but we want to support our users to install the required applications if they don't have them installed already. 

We are looking at providing apps as GitLab Integrated Apps if they satisfy the following criteria:

- Each GitLab Integrated App should provide sane, production-ready default configurations.
- Each GitLab Integrated App should be a requirement for current or planned GitLab functionality or it should integrate deeply with GitLab.
- Each GitLab Integrated App should have a clear support path, possibly outside of GitLab.

We believe that infrastructure, including applications, should be managed by code whenever possible. As a result, we provide the [cluster management project](https://docs.gitlab.com/ee/user/clusters/management_project.html) to get started easily with the GitLab Integrated Apps.

## Roadmap

A more detailed view into [our roadmap is maintained within GitLab](https://gitlab.com/groups/gitlab-org/-/roadmap?state=opened&sort=end_date_asc&label_name%5B%5D=group%3A%3Aconfigure&label_name%5B%5D=Roadmap).

## Challenges

We see Kubernetes as a platform for delivering containerised software to users. Currently, every company builds their own workflows on top of it. The biggest challenges as we see them are the following:

- Making Kubernetes developer friendly is hard. We want to make deployments to every Kubernetes environment to be frictionless for the developer and reliable for the operator.
- As clusters are complex, having an overview of the state and contents of a cluster is hard. We want to help our [monitoring efforts](/direction/monitor/) to provide clean visualisations and a rich understanding of one's cluster.

## Approach

- Kubernetes has a large and engaged community, and we want to build on the knowledge and wisdom of the community, instead of re-inventing existing solutions.
- In Kubernetes there are a plethora of ways for achieving a given goal. We want to provide a default setup and configuration options to integrate with popular approaches and tools.

## Maturity

Kubernetes Management is currently `Viable`. We want to make it `Complete` by June 2021.

## Competitive landscape

### IBM Cloud Native Toolkit

It provides an Open Source Software Development Life Cycle (SDLC) and complements IBM Cloud Pak solutions. The Cloud Native Toolkit enables application development teams to deliver business value quickly using Red Hat OpenShift and Kubernetes on IBM Cloud.

### Spinnaker

Spinnaker is an open-source, multi-cloud continuous delivery platform that helps you release software changes with high velocity and confidence. It provides two core sets of features 1) Application management, and 2) Application deployment.

### Backstage

Backstage.io with its service registry oriented approach and kubernetes integration provides an umbrella to integrate other DevOps tools and provides some insights required by developers of Kubernetes workloads.

### Argo CD

Argo CD is often considered to be the leading GitOps tool for Kubernetes with an outstanding UI.

## Analyst landscape

This category doesn't quite fit the "configuration management" area as it relates only to Kubernetes. No analyst area currently defined. 

## Top Customer Success/Sales issue(s)

[Customize Kubernetes namespace per environment for managed clusters](https://gitlab.com/gitlab-org/gitlab/-/issues/38054)

## Top user issue(s)

- [Investigate a recommended way to set up GitLab Integrated Applications](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/issues/104)
- [Simplify getting started with the Agent](https://gitlab.com/groups/gitlab-org/-/epics/5786)

## Top internal customer issue(s)

## Top Vision Item(s)

- [Kubernetes Management - enterprise level K8s integration](https://gitlab.com/gitlab-org/gitlab/-/issues/216569)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/595)
