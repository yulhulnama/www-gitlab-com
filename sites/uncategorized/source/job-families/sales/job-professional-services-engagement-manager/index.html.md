---
layout: job_family_page
title: "Professional Services Engagement Manager"
---

The Engagement Manager plays the critical role for setting customers up for Success by prescriptively shaping and then leading the execution of large and transformational projects at our most strategic customers. The Engagement Manager serves as a trusted advisor to customers in their portfolio, providing guidance to Sales and Professional Services teams, ensuring synergies across each customer’s projects, and ensuring the highest levels of customer satisfaction.

## Levels

### Professional Services Engagement Manager (Intermediate)

The Professional Services Engagement Manager (Intermediate) reports to the Sr. Director, Professional Services.

#### Professional Services Engagement Manager (Intermediate) Job Grade 

The Professional Services Engagement Manager (Intermediate) is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Professional Services Engagement Manager (Intermediate) Responsibilities

* Align closely with Account Sales teams to own pre-sales activities such as scoping, solutioning, SOW development and project staffing
* Lead the development of client-specific implementation proposals, SOWs, staffing plans, engaging with SMEs across the organization to gain consensus on an acceptable proposal
* Provide the Professional Services team with Subject Matter Expertise related to the proposed solution and client needs in order to ensure successful project delivery
* Manage project- and account-level escalations as needed
* Anticipate needs and position training, support and other solutions that may be needed for a successful customer experience
* Work closely with Project Managers to ensure that engagements conclude with fully satisfied clients that are willing to be references for new potential clients
* Deep understanding of factors that drive customer success for Contact Center implementations and how they directly contribute to long term customer retention
* The ability to proactively identify and mitigate risks to customer success, be it through the addition of new products and services, strategy & planning, or escalation
* Travel 20-40% (depends on geography and account distribution)

#### Professional Services Engagement Manager (Intermediate) Requirements

* 5+ years experience delivering consulting services, including team leadership and active involvement in selling professional services
* 3+ years managing C-level client relationships, including escalation resolution
* 3+ years of enterprise-level project management experience
* 3+ years operating in a pre-sales environment, shaping and scoping large and complex implementation projects
* Extremely strong written and verbal communication skills, executive level presence and experience in working in a client advisory role
* Able to command a group audience, facilitate solutioning and lead discussions such as implementation methodology, road mapping, enterprise strategy and executive-level requirement gathering sessions
* Highly developed soft skills, with the ability to adjust communication style based on the audience and difficult client situations.
* Excellent analytical & problem solving skills.
* Collaborative and consultative work style, ability to thrive in a high velocity, highly dynamic work environment
* History of working in a consultative selling environment, where clients seek and value your opinions and see your advice as objective and unbiased
* BA/BS or equivalent; MBA is a plus
* * You share our values, and work in accordance with those values.
* [Leadership at GitLab](/handbook/leadership/)
* Ability to use GitLab

### Senior Professional Services Engagement Manager

The Senior Professional Services Engagement Manager reports to the Sr. Director, Professional Services.

#### Senior Professional Services Engagement Manager Job Grade 

The Professional Services Engagement Manager (Senior) is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Professional Services Engagement Manager Responsibilities

* Extends that of the Professional Services Engagement Manager (Intermediate) responsibilities
* Cultivate knowledge of GitLab's solutions and emerging trends in DevSecOps
* Guide the implementation and sales team by finding solutions and delivery personnel to address issues and challenges to meet customer requirements for high priority / high visibility project
* Determine methodologies and procedures on new assignments based on general direction from senior management

#### Senior Professional Services Engagement Manager Requirements

* Extends that of the Professional Services Engagement Manager (Intermediate) requirements
* 7+ years experience delivering consulting services, including team leadership and active involvement in selling professional services
* 5+ years managing C-level client relationships, including escalation resolution
* 5+ years of enterprise-level project management experience
* 7+ years operating in a pre-sales environment, shaping and scoping large and complex implementation projects
* People management preferred, but not required

## Performance Indicators

* [Bookings per agreed plan (65%)](/handbook/sales/#closed-deal---won)
* [Revenue per agreed plan (35%)](/handbook/sales/#pcv)

## Career Ladder

The Professional Services Engagment Manager Job Family would move into the [Professional Services Engineer](/job-families/sales/professional-services-engineer/) Job Family or the [Professional Services Practice Manager](/job-families/sales/professional-services-practice-manager/) Job Family.

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process.

1. Phone screen with a GitLab Recruiting Team Member
2. Video Interview with the Hiring Manager
3. Team Interviews with 1-4 Team Members

Additional details about our process can be found on our [hiring page](/handbook/hiring).
